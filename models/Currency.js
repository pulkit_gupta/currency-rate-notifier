
/**
 * Created by pulkitgupta on 22/04/2016.
 */

// Currency model
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CurrencySchema = new Schema({
    name: String
});

module.exports = mongoose.model('Currency', CurrencySchema);

