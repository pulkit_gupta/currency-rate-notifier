/**
 * Created by pulkitgupta on 22/04/2016.
 */

// User model
var mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
    Schema = mongoose.Schema;

var offerSchema = new Schema({

    base_cur: {type: String, default: 'EUR'},
    cur_code: String,
    name: String,
    bid: Number,
    last_value: Number,
    last_fetch: Date,
    last_modified: { type: Date, default: Date.now }
});

autoIncrement.initialize(mongoose);

//offerSchema.plugin(autoIncrement.plugin, 'offerSchema');

var UserSchema = new Schema({
    name: String,
    email: String,
    reg_date: { type: Date, default: Date.now },
    //embedded for better performance and one to few relationship, can be done with foreign key concept.
    offers: [offerSchema]
});

UserSchema.plugin(autoIncrement.plugin, {
    model: 'User', // The model to configure the plugin for.
    field: '_id', // The field the plugin should track.
    startAt: 1, // The number the count should start at.
    incrementBy: 1, // The number by which to increment the count each time.
    unique: true // Should we create a unique index for the field
});

//
//UserSchema.virtual('date')
//    .get(function(){
//        return this._id.getTimestamp();
//    });

module.exports = mongoose.model('User', UserSchema);

