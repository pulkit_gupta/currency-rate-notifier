var config = require('./config');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var users = require('./routes/users');
var offers = require('./routes/offers');
var mongoose = require('mongoose');
var app = express();

mongoose.connect(config.db);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', express.static(__dirname + '/public'));

app.use('/user', users);
app.use('/offer', offers);

app.get('/', function(req, res){res.sendFile(__dirname+'/public/index.html')});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.status(404).send("Not found");
  //next(err);
});

app.use(function (err, req, res, next){
    res.status(500).send({ error: 'Something failed!' });
    next(err);
});


//Node End error handler for developers

app.use(function(err, req, res, next){
  console.error("This is the error handling"+err.stack);
});

process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log(err.stack);
});
module.exports = app;
