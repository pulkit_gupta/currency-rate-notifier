# README #

This file contains all the steps to run and use the app (Currency Rate Notifier). This app is based on RestFul API. It uses Forex API to get the real time currency conversion value.
There is also a small front-end in HTML and vanilla-js for testing purpose.

### Setup and install ###

* Download and install NPM and Nodejs from http://nodejs.org 
* Download and install MongoDB from https://www.mongodb.org/downloads#production
* Download or clone the repository
* Run 'npm install' to install all the packages mentioned in Package.json
* Open the root/config.js and change the name of DB, http port and credentials for Forex and Pusher dependencies. (For desired environment)
* Run 'npm start' to start server (Browser will be launched automatically)

* Optional: To debug run 'npm run-script dev'

### Usage (Frontend) ###

* Enter the user name, email and click register to receive a user id.
* Use this userid, currency and bid to enter the offers
* Use this user id to bind the push event from server.

User will receive notification if Bid value is less than actual value calculated by server at x interval set at the server.

### RestFul services ###

* Refer to Guide_RestFulAPI.pdf in root folder.
* Download Postman (Chrome extension for Http requests)
* Import Request file root/currency-notifer.json.postman_collection to download all the request settings in your postman.
* Change the data in tabs and hit desired endpoints.

### Contact ###

* Pulkit Guta (Author)
* pulkit.itp@gmail.com