/**
 * Created by pulkitgupta on 22/04/2016.
 */


var path = require('path'),
    rootPath = path.normalize(__dirname + '/'),
    env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: "currency-notifier"
        },
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost/cnDB14',
        forex: {
            url: "http://globalcurrencies.xignite.com/xGlobalCurrencies.json",
            token: "3922A297A44B40CCA4A241E1CE2F2B30"
        },
        pusher: {
            appId: "200647",
            key: "c592fc62667946d1e19a",
            secret: "20fe3282f5ad668be3b7",
            cluster: 'eu',
            encrypted: true,
            events: {
                notify: "notify"
            }
        }
    },

    test: {
        root: rootPath,
        app: {
            name: "currency-notifier"
        },
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost/cnDB14',
        forex: {
            url: "http://globalcurrencies.xignite.com/xGlobalCurrencies.json",
            token: "3922A297A44B40CCA4A241E1CE2F2B30"
        },
        pusher: {
            appId: "200647",
            key: "c592fc62667946d1e19a",
            secret: "20fe3282f5ad668be3b7",
            cluster: 'eu',
            encrypted: true,
            events: {
                notify: "notify"
            }
        }
    },

    production: {
        root: rootPath,
        app: {
            name: "currency-notifier"
        },
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost/cnDB14',
        forex: {
            url: "http://globalcurrencies.xignite.com/xGlobalCurrencies.json",
            token: "3922A297A44B40CCA4A241E1CE2F2B30"
        },
        pusher: {
            appId: "200647",
            key: "c592fc62667946d1e19a",
            secret: "20fe3282f5ad668be3b7",
            cluster: 'eu',
            encrypted: true,
            events: {
                notify: "notify"
            }
        }
    }
};

module.exports = config[env];
