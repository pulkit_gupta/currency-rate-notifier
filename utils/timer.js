/**
 * Created by pulkitgupta on 24/04/2016.
 */

var Forex = require('./forexApi');
var User = require('../models/User');
var Currency = require('../models/Currency');
var PushNotification = require('./pushNotification');

var getAllUserOffers = function(callback){
    User.find({}, {offers: 1}, function(err, users){
        if(err){
            res.status(500).send("Something broke!");
            throw err;
        }

        callback(users)
    });
};


var saveUpdateInDB = function(uid, offers){
    //update in DB
    User.findOneAndUpdate({_id: uid }, {$set: {offers: offers}}, function(err, data){

        if(err){
            console.log(err);
            throw err;
        }

        console.log("Successfully updated in DB");

    });
};


var pushToUser = function(uid, dataArray){

    var channel = "user-"+uid;
    var msg = {data: dataArray};
    console.log("Pushing the value"+ msg);
    PushNotification.send(channel, msg);

};


var getAllCurrencies = function(callback){

    Currency.find({}, function(err, data){
        if(err){
            throw err;
        }
       callback(data);
    });

};


var getLatestRate = function(data){

    var csvCode =  "";
    data.forEach(function(elem){
        csvCode += elem.name + ",";
    });
    csvCode = csvCode.substr(0, csvCode.length-1);
    console.log("csv code is "+ csvCode);

    var storedRates = []; //{conv_code: "", bid: ""}

    if(csvCode == "")
    {
        console.log("No currencies");
        return;
    }
    Forex.getCurrency(csvCode, function(data){


        if(data == null){
            console.log("failed");
            return;
        }

        if(Array.isArray(data)){
            console.log("Response is Array");

            data.forEach(function(rate){
                console.log("value stored is "+ rate.Bid+ " at "+ rate.Symbol);
                storedRates[rate.Symbol] = rate.Bid;

            });
        }
        else{
            console.log("value stored is "+ data.Bid+ " at "+ data.Symbol);
            storedRates[data.Symbol] = data.Bid;
        }


        getAllUserOffers(function(users){


             users.forEach(function(user){
                var toPushArray = [];
                user.offers.forEach(function(offer){

                    var value = storedRates[offer.name];
                    console.log("value from store for"+offer.name+" is "+ value);

                    offer.last_value = value;
                    offer.last_fetch = new Date();

                    console.log("bid balues are "+ offer.last_value+ " > "+ offer.bid);
                    if(offer.last_value > offer.bid){
                        toPushArray.push({code: offer.name, value: offer.last_value});
                    }
                });

                //save the DB
                saveUpdateInDB(user._id, user.offers);
                if(toPushArray.length >  0){
                    pushToUser(user._id, toPushArray);
                }
                else{
                    console.log("pushTouser is empty.");
                }

            });

        });

    });


};

var timer = function(seconds){


    console.log("Timer started for the interval of "+seconds + " seconds");

    setInterval(function(){
        getAllCurrencies(getLatestRate);
    }, seconds*1000);

};

module.exports.start = timer;
