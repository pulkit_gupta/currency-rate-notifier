/**
 * Created by pulkitgupta on 24/04/2016.
 */


var Pusher = require('pusher');
var config = require('../config');

var pusher = new Pusher({
    appId: config.pusher.appId,
    key: config.pusher.key,
    secret: config.pusher.secret,
    cluster: config.pusher.cluster,
    encrypted: config.pusher.encrypted
});

module.exports.send = function(channel, messageObj){
    pusher.trigger(channel, config.pusher.events.notify, messageObj);
};
