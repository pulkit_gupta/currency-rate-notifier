var http = require('http');
var config = require('../config');


var getCurrency = function(conversionCode, callback){

    var dependentURL = conversionCode.indexOf(',') === -1 ? "/GetRealTimeRate?Symbol": "/GetRealTimeRates?Symbols";

    var url = config.forex.url + dependentURL+'='+conversionCode+'&_token='+config.forex.token;
    console.log(url);

    http.get(url, function(res) {

        var body = '';

        res.on('data', function(chunk){
            body += chunk;
        });

        res.on('end', function(){
            var forexResp = JSON.parse(body);
            callback(forexResp);
        });

    });
};


module.exports.getCurrency = getCurrency;


