/**
 * Created by pulkitgupta on 24/04/2016.
 */

(function(){

    var start = function(){

        console.log("on load is being called with listener on"+ $('#register'));
        var nameObj = $('#name');
        var emailObj = $('#email');

        $( "#listen" ).click(function() {

            var userchannel = 'user-'+$('#uid').val();
            $('#listen-output').text("Your listner is created on channel: "+userchannel+ " Please wait for any notification.");
            console.log("user channel is "+ userchannel);

            var channel = app.pusher.subscribe(userchannel);


            channel.bind('notify', function(obj) {

                //$('#notification').append("<p>Value for your offer "+obj.code+" is "+ obj.value+"</p>");

                obj.data.forEach(function(elem){
                    $('#notification').append("<p> Time: "+new Date()+"=> Value for your offer "+elem.code+" is "+ elem.value+"</p>");
                });

            });

        });


        $( "#register" ).click(function() {

            var name = nameObj.val();
            var email = emailObj.val();
            console.log(name);
            console.log(email);

            $.post( "http://localhost:3000/user",
                { name: name, email: email },

                function(response){
                    $('#reg-output').text("Welcome "+ name+ " ! your user ID is "+ response.uid);
                }
            );

        });



        $( "#offer-add" ).click(function() {
            var uid = $('#offer-uid').val();
            var bid = $('#bid').val();
            var cur = $('#cur').val();

            $.post( "http://localhost:3000/offer?uid="+uid,
                { cur_code: cur, bid: bid },

                function(response){
                    $('#offer-output').append("<p>Added the conversion code "+ response.data.name+ " with bid value = "+ response.data.bid+"</p>");
                }
            );

        });
    };

    window.onload = start;
})();
