/**
 * Created by pulkitgupta on 23/04/2016.
 */


var express = require('express');
var router = express.Router();
var User = require('../models/User');
/* GET home page. */
router.get('/:uid', function(req, res, next) {
    console.log(req.params.uid);
    User.find({_id: req.params.uid},function(err, users){
        if(err){
            res.status(500).send("Something broke!");
            throw err;
        }
        res.send(users);

    });
});

/**
 *  Send all users in Database with all of their information.
 */
router.get('/', function(req, res, next) {

    User.find(function(err, users){
        if(err){
            res.status(500).send("Something broke!");
            throw err;
        }
        res.send(users);
    });
});

/**
 * Register a new user with URL encoded values name and email.
 * Response contains status, userID and authToken (Optional)
 */
router.post('/', function(req, res){

    var name = req.body.name, email = req.body.email;

    if(name== undefined || name == "" || email == undefined || email == ""){

        res.status(400).send("Name and email fields are mandatory.");

    }else{

        console.log(name);
        console.log(email);

        var newUser = new User({name: name, email: email});


        newUser.save(function(err, data){
            if(err)
                throw err;
            res.send({status: "success", uid: data._id, authToken: ""});

        });
    }


});

module.exports = router;
