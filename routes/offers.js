var express = require('express');
var router = express.Router();
var User = require('../models/User');
var Currency = require('../models/Currency');

/* GET users listing. */

var requiredUID = function(req, res, next){
  if(req.query.uid == undefined){
    res.status(400).send("user id is missing.");
  }
  else{
    next();
  }
};

router.get('/',requiredUID, function(req, res, next) {

  User.find({_id: req.query.uid},{offers: 1}, function(err, data){
    if(err){
      res.status(500).send("Something broke!");
      throw err;
    }

    var status = "success";
    if(data.length ==0){
      status = "No data";
    }
    res.send({status: status, data: data});
  });
});




var getDuplicateAndUpdate = function(uid, name, callback){

  User.findOne({_id: uid}, {offers: 1},  function(err, data){
      if(err){
        throw err;
      }
      var toReturn = null;
      var isDup = false, index = 0;
      //console.log(data);
      if(data){

        for(var i = 0; i< data.offers.length; i++){
          console.log(data.offers[i].name);
          if(data.offers[i].name == name){
            //console.log("found already.");
            //data.offers[i].value = value;
            index = i;
            isDup = true;
            break;
          }
        }


        if(isDup){
          toReturn = data.offers;
        }
      }

    callback(toReturn, index);


  });

};

router.put('/', function(req, res, next){

  var cur_code = req.body.cur_code;
  var bid =  req.body.bid;
  console.log("currency is "+ cur_code+ " with value of "+ bid);

  var name = 'EUR'+cur_code;

  getDuplicateAndUpdate(req.query.uid, name, function(offer, index){

      if(!offer){
        res.status(409).json({status: "Not found", data: null});
      }
      else{

        //update here.
        offer[index].bid = bid;
        offer[index].last_modified = new Date();

        User.findOneAndUpdate({_id: req.query.uid }, {$set: {offers: offer}}, function(err, data){

          if(err){
            res.status(500).send("Something broke!");
            throw err;
          }
          var status = "success";
          res.send({status: status, data: offer[index]});

        });

      }

  });

});



router.post('/', function(req, res, next){

  var cur_code = req.body.cur_code;
  var bid =  req.body.bid;


  console.log("currency is "+ cur_code+ " with value of "+ bid);

  var name = 'EUR'+cur_code;
  getDuplicateAndUpdate(req.query.uid, name, function(offer, index){

    if(offer){
      res.status(409).json({status: "Bid already present", data: offer[index]});
    }
    else{

      var newCurrency = new Currency({name: name});

      Currency.find({name : name}, function (err, docs) {
        if (!docs.length){
          newCurrency.save();
        }else{
          console.log('currency exists: ', name);
        }
      });



      var newOffer = {cur_code: cur_code, bid: bid, name: name, last_fetch: new Date(), last_value: 0};
      User.findOneAndUpdate({_id: req.query.uid }, {$push: {offers: newOffer}},{"upsert": true}, function(err, data){

        if(err){
          res.status(500).send("Something broke!");
          throw err;
        }
        var status = "success";
        res.send({status: status, data: newOffer});

      });

    }

  });

});






module.exports = router;
